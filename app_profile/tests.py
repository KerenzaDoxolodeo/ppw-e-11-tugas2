from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here.

class ProfileUnitTest(TestCase):

    def Profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def Profile_is_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
