"""paketa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic import RedirectView
from django.contrib import admin
import riwayat.urls as riwayat
import app_profile.urls as profile
import teman.urls as teman
import loginstatus.urls as loginstatus

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^riwayat/', include(riwayat, namespace='riwayat')),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^$', RedirectView.as_view(permanent='True', url='/profile/')),
    url(r'^teman/', include(teman, namespace='teman')),
    url(r'^loginstatus/', include(loginstatus, namespace='loginstatus')),
]
