from django.shortcuts import render
import requests

response = {}

def index(request):
    if 'nilaiMahasiswa' not in request.session:
        request.session['nilaiMahasiswa'] = True
    response['secret'] = request.session['nilaiMahasiswa']
    html = 'riwayat/riwayat.html'
    response['data'] = get_detail_riwayat()
    return render(request, html, response)

def get_detail_riwayat():
    url = "https://private-e52a5-ppw2017.apiary-mock.com/riwayat"
    req = requests.get(url)
    rj = req.json()

    return rj
