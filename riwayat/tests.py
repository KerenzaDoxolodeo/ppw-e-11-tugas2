from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class LabRiwayatUnitTest(TestCase):
    def test_detail_riwayat_json(self):
        m = get_detail_riwayat()
        self.assertTrue(len(m)==37)

    def test_riwayat_is_working(self):
        response = self.client.get('/riwayat/')
        html_response = response.content.decode('utf8')
        self.assertTrue(self.client.session['nilaiMahasiswa'] == True)
        self.assertIn('Rahasia',html_response)

        session = self.client.session
        session['nileiMahasiswa'] = False
        session.save()

        response = self.client.get('/riwayat/')
        html_response = response.content.decode('utf8')
        self.assertIn('A-',html_response)

        session = self.client.session
        session['nilaiMahasiswa'] = True
        session.save()

        response = self.client.get('/riwayat/')
        html_response = response.content.decode('utf8')
        self.assertIn('Rahasia',html_response)

    def test_riwayat_using_index_func(self):
        found = resolve('/riwayat/')
        self.assertEqual(found.func, index)
        


