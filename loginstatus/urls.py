from django.conf.urls import url

from .views import index,status,set_data_for_session
from .custom_auth import auth_login, auth_logout

#url for app
urlpatterns = [
        url(r'^$', index, name='index'),
    url(r'^status/$', status, name='status'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]
