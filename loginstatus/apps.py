from django.apps import AppConfig


class LoginstatusConfig(AppConfig):
    name = 'loginstatus'
