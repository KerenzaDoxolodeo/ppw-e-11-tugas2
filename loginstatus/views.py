from django.shortcuts import render

# Create your views here.

response = {}

def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('loginstatus:status'))
    else:
        html = 'loginstatus/pralogin.html'
        return render(request, html, response)

def status(request):
    print ("#==> status")
    
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('loginstatus:index'))

    set_data_for_session(response)

    html = 'loginstatus/status.html'
    return render(request, html, response)

def set_data_for_session(request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['login'] = True

